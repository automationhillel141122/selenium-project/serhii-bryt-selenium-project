package org.bryt.tests;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.BasePage;
import org.bryt.pages.LoginPage;
import org.bryt.utils.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginLogoutTests extends BaseTest{

    String user = PropertyReader.getProperty("user.name");
    String password = PropertyReader.getProperty("user.password");

    String errorMessage = "Login was unsuccessful. Please correct the errors and try again.\n" +
            "The credentials provided are incorrect";

    String errorMessageNoUser = "Login was unsuccessful. Please correct the errors and try again.\n" +
            "No customer account found";

    String mailError = "Please enter your email";

    @BeforeMethod
    public void beforeMethod() {
        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));
        new BasePage()
                .mainMenu()
                .selectLogin();
    }

    @Test(dataProvider = "dataProvider")
    public void incorrectLoginPage(String userEmail, String userPassword, String errorMessage, String nameErrorMessage) {
        LoginPage loginPage = new LoginPage()
                .unSuccessfulLogin(userEmail, userPassword);

        if (errorMessage != null){
            Assert.assertEquals(loginPage.getSummaryErrorText(), errorMessage);
        }

        if (nameErrorMessage != null){
            Assert.assertEquals(loginPage.getEmailErrorText(), nameErrorMessage);
        }

    }

    @DataProvider
    public Object[][] dataProvider(){
        return new Object[][]{
                {"", "", null, mailError},
                {user, "", errorMessage, null},
                {"", password, null, mailError},
                {user + "1", password, errorMessageNoUser, null},
                {user, password + "0", errorMessage, null},
                {user + "1", password + "0", errorMessageNoUser, null}
        };
    }

}
