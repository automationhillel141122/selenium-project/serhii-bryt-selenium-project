package org.bryt.tests;

import org.bryt.driver.WebdriverHolder;
import org.bryt.steps.NavigationSteps;
import org.bryt.steps.ProductActionsSteps;
import org.bryt.utils.PropertyReader;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BaseTest {

    protected NavigationSteps navigationSteps = new NavigationSteps();
    protected ProductActionsSteps productActionsSteps = new ProductActionsSteps();

    @AfterSuite
    public void afterSuite() {
        WebdriverHolder.getInstance().quitDriver();
    }

    @BeforeMethod
    public void beforeMethodGlobal() {
        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));

    }
}
