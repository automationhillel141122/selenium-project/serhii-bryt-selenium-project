package org.bryt.tests;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.BasePage;
import org.bryt.pages.products.ProductModel;
import org.bryt.pages.products.ProductsPage;
import org.bryt.pages.products.SortingDirection;
import org.bryt.utils.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingTests extends BaseTest {

    @Test
    public void sortZtoATest() {
        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));
        ProductsPage productsPage = new BasePage()
                .mainMenu()
                .selectLogin()
                .login()
                .categoryMenu()
                .selectSubCategory("Computers", "Notebooks");

        List<ProductModel> productsBeforeSorting = productsPage.getProducts();
        List<ProductModel> productsAfterSorting = productsPage
                .sort(SortingDirection.PRICE_HIGH_TO_LOW)
                .getProducts();

        Collections.sort(productsBeforeSorting, new Comparator<ProductModel>() {
            @Override
            public int compare(ProductModel o1, ProductModel o2) {
                return o2.price().compareTo(o1.price());
            }
        });

        Assert.assertEquals(productsBeforeSorting, productsAfterSorting);

    }
}
