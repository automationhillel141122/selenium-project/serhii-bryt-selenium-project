package org.bryt.tests;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.BasePage;
import org.bryt.pages.products.ProductModel;
import org.bryt.pages.products.ProductsPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

public class NavigationTests extends BaseTest {


    @AfterMethod
    public void afterMethod(){
        WebdriverHolder.getInstance().quitDriver();
    }

    @Test
    public void navigateCategories() {
        ProductsPage productsPage = new BasePage()
                .categoryMenu()
                .selectSubCategory("Computers", "Desktops");

        int itemsCount = productsPage.getItemsCount();
        Assert.assertEquals(itemsCount, 3);


        productsPage
                .categoryMenu()
                .selectMainCategory("Books");
        productsPage = new ProductsPage();

        itemsCount = productsPage.getItemsCount();
        Assert.assertEquals(itemsCount, 3);

        List<ProductModel> products = productsPage.getProducts();
        System.out.println(products);


    }

    @Test
    public void addItemToCart() {
        productActionsSteps
                .addProductToCart("Computers", "Notebooks", "Samsung Series 9 NP900X4C Premium Ultrabook");
        System.out.println();
    }

}
