package org.bryt.tests;

import com.codeborne.selenide.Configuration;
import org.bryt.pages.selenide.BasePage;
import org.bryt.pages.selenide.LoginPage;
import org.bryt.pages.selenide.products.ProductsPage;
import org.bryt.utils.PropertyReader;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Map;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class LoginLogoutTestsSelenide extends BaseTest {

    String user = PropertyReader.getProperty("user.name");
    String password = PropertyReader.getProperty("user.password");

    String errorMessage = "Login was unsuccessful. Please correct the errors and try again.\n" +
            "The credentials provided are incorrect";

    String errorMessageNoUser = "Login was unsuccessful. Please correct the errors and try again.\n" +
            "No customer account found";

    String mailError = "Please enter your email";

    @BeforeMethod
    public void beforeMethod() {
        Configuration.timeout = 15000;
        Configuration.remote = "http://localhost:4444/wd/hub";
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", "chrome");
        capabilities.setCapability("selenoid:options", Map.<String, Object>of(
                "enableVNC", true
        ));
        Configuration.browserCapabilities = capabilities;
        open(PropertyReader.getProperty("base.url"));
        new BasePage()
                .mainMenu()
                .selectLogin();
    }

    @Test(dataProvider = "dataProvider")
    public void incorrectLoginPage(String userEmail, String userPassword, String errorMessage, String nameErrorMessage) {
        LoginPage loginPage = new LoginPage()
                .unSuccessfulLogin(userEmail, userPassword);

        if (errorMessage != null) {
            Assert.assertEquals(loginPage.getSummaryErrorText(), errorMessage);
        }

        if (nameErrorMessage != null) {
            Assert.assertEquals(loginPage.getEmailErrorText(), nameErrorMessage);
        }
        sleep(5000);

    }

    @Test
    public void downloadTest() {
        new LoginPage()
                .login(user, password)
                .categoryMenu()
                .selectMainCategory("Digital downloads");
        File file = new ProductsPage()
                .selectProduct("Night Visions")
                .downloadSample();
        System.out.println(file.getName());
    }


    @DataProvider
    public Object[][] dataProvider() {
        return new Object[][]{
                {"", "", null, mailError},
                {user, "", errorMessage, null},
                {"", password, null, mailError},
                {user + "1", password, errorMessageNoUser, null},
                {user, password + "0", errorMessage, null},
                {user + "1", password + "0", errorMessageNoUser, null}
        };
    }

}
