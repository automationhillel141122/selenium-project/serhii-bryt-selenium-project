package org.bryt.tests;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.BasePage;
import org.bryt.pages.products.ProductModel;
import org.bryt.pages.products.ProductsPage;
import org.bryt.utils.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CompareTests extends BaseTest{

    @AfterMethod
    public void afterMethod(){
        WebdriverHolder.getInstance().quitDriver();
    }

    @Test
    public void compareTest(){
//        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));
        ProductsPage productsPage = new BasePage()
                .mainMenu()
                .selectLogin()
                .login()
                .categoryMenu()
                .selectSubCategory("Computers", "Notebooks");

        String product1 = "Lenovo Thinkpad X1 Carbon Laptop";
        String product2 = "Asus N551JK-XO076H Laptop";

        List<ProductModel> products = productsPage
                .addProductToCompare(product1)
                .addProductToCompare(product2)
                .goToComparePage()
                .getProducts();

        Assert.assertEquals(products.size(), 2);

        List<String> names = new ArrayList<>();
        for (ProductModel model: products){
            names.add(model.productName());
        }

        Assert.assertTrue(names.contains(product1));
        Assert.assertTrue(names.contains(product2));
    }


    @Test
    public void compareTest1(){
//        WebdriverHolder.getInstance().getDriver().get(PropertyReader.getProperty("base.url"));
        ProductsPage productsPage = new BasePage()
                .mainMenu()
                .selectLogin()
                .login()
                .categoryMenu()
                .selectSubCategory("Computers", "Notebooks");

        String product1 = "Lenovo Thinkpad X1 Carbon Laptop";
        String product2 = "Samsung Series 9 NP900X4C Premium Ultrabook";

        List<ProductModel> products = productsPage
                .addProductToCompare(product1)
                .addProductToCompare(product2)
                .goToComparePage()
                .getProducts();

        Assert.assertEquals(products.size(), 2);

        List<String> names = new ArrayList<>();
        for (ProductModel model: products){
            names.add(model.productName());
        }

        Assert.assertTrue(names.contains(product1));
        Assert.assertTrue(names.contains(product2));
    }
}
