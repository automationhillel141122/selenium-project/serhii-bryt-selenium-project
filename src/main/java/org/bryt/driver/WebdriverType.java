package org.bryt.driver;

public enum WebdriverType {
    CHROME,
    FIREFOX,
    SAFARI
}
