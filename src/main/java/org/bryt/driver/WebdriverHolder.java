package org.bryt.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WebdriverHolder {
    private static WebdriverHolder instance = null;
    private WebDriver driver;
    private WebDriverWait waiter;

    private WebdriverHolder() {
        initDriver();
    }

    public synchronized static WebdriverHolder getInstance() {
        if (instance == null) {
            instance = new WebdriverHolder();
        } else {
            if (instance.getDriver() == null){
                instance = new WebdriverHolder();
            }
        }
        return instance;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWaiter() {
        return waiter;
    }

    private void initDriver() {
        this.driver = WebDriverFactory.initDriver();
        this.waiter = new WebDriverWait(this.driver, Duration.ofSeconds(15));
    }

    public void quitDriver(){
        driver.quit();
        driver = null;
    }

}
