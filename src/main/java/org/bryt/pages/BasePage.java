package org.bryt.pages;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.category_menu.CategoryMenu;
import org.bryt.pages.compare.ComparePage;
import org.bryt.pages.main_menu.MainMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class BasePage {

    public BasePage() {
        PageFactory.initElements(WebdriverHolder.getInstance().getDriver(), this);
    }

    public MainMenu mainMenu(){
        return new MainMenu();
    }

    public CategoryMenu categoryMenu(){
        return new CategoryMenu();
    }

    public BasePage selectCurrency(AppCurrency currency){
        WebElement customerCurrency = WebdriverHolder.getInstance().getDriver()
                .findElement(By.id("customerCurrency"));
        new Select(customerCurrency)
                .selectByVisibleText(currency.currencyText());
        return new BasePage();
    }

    public BasePage search(String searchPattern){
        WebElement searchForm = WebdriverHolder.getInstance().getDriver().findElement(By.id("small-search-box-form"));
        searchForm.findElement(By.xpath(".//input")).sendKeys(searchPattern);
        searchForm.findElement(By.xpath(".//button")).click();
        return new BasePage();
    }

    public ComparePage goToComparePage(){
        WebdriverHolder.getInstance().getDriver().findElement(By.linkText("Compare products list")).click();
        return new ComparePage();
    }

    protected void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
