package org.bryt.pages.selenide;

public enum AppCurrency {
    USD("US Dollar"),
    EUR("Euro"),
    JPE("Japanese Yen");

    private String currencyText;

    AppCurrency(String currencyText) {
        this.currencyText = currencyText;
    }

    public String currencyText() {
        return currencyText;
    }
}
