package org.bryt.pages.selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends BasePage {
    SelenideElement emailInput = $(byId("Email"));
    SelenideElement passwordInput = $(byId("Password"));
    SelenideElement rememberMeCheckBox = $(byId("RememberMe"));
    SelenideElement loginButton = $(".button-1.login-button");

    public LoginPage() {
        super();
    }

    public BasePage login(String email, String password, boolean... rememberMe) {
        enterCredentialsAndLogin(email, password, rememberMe);
        return new BasePage();
    }

    public LoginPage unSuccessfulLogin(String email, String password, boolean... rememberMe) {
        enterCredentialsAndLogin(email, password, rememberMe);
        return new LoginPage();
    }

    private void enterCredentialsAndLogin(String email, String password, boolean... rememberMe) {
        emailInput.setValue(email);
        passwordInput.setValue(password);
        if (rememberMe.length > 0) {
            selectRememberMe(rememberMe[0]);
        }
        loginButton.click();
    }

    public String getEmailErrorText() {
        return $(By.id("Email-error")).text();
    }

    public String getSummaryErrorText() {
        return $(".message-error.validation-summary-errors").text();
    }


    private void selectRememberMe(boolean rememberMe) {
        if (rememberMe) {
            if (rememberMeCheckBox.is(Condition.not(Condition.checked))) {
                rememberMeCheckBox.click();
            }
        } else {
            if (rememberMeCheckBox.is(Condition.checked)) {
                rememberMeCheckBox.click();
            }
        }
    }
}
