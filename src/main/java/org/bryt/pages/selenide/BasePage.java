package org.bryt.pages.selenide;

import com.codeborne.selenide.SelenideElement;
import org.bryt.pages.selenide.category_menu.CategoryMenu;
import org.bryt.pages.selenide.main_menu.MainMenu;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

public class BasePage {

    public BasePage() {
    }

    public MainMenu mainMenu(){
        return new MainMenu();
    }

    public CategoryMenu categoryMenu(){
        return new CategoryMenu();
    }

    public BasePage selectCurrency(AppCurrency currency){
        $(byId("customerCurrency"))
                .selectOption(currency.currencyText());
        return new BasePage();
    }

    public BasePage search(String searchPattern){
        SelenideElement searchForm = $(byId("small-search-box-form"));
        searchForm.$x(".//input").setValue(searchPattern);
        searchForm.$x(".//button").click();
        return new BasePage();
    }
}
