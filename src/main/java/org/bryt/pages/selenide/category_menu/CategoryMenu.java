package org.bryt.pages.selenide.category_menu;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.bryt.pages.selenide.BasePage;
import org.bryt.pages.selenide.products.ProductsPage;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.actions;

public class CategoryMenu {
    public BasePage selectMainCategory(String mainCategory) {
        $$("ul.top-menu.notmobile>li>a")
                .find(Condition.text(mainCategory))
                .click();
        return new BasePage();
    }

    public ProductsPage selectSubCategory(String mainCategory, String subCategory) {
        SelenideElement mainCategoryElement = $$("ul.top-menu.notmobile>li>a")
                .find(Condition.text(mainCategory));
        actions().moveToElement(mainCategoryElement).build().perform();

        mainCategoryElement
                .parent()
                .$$x(".//ul[@class='sublist first-level']/li/a")
                .find(Condition.text(subCategory))
                .click();
        return new ProductsPage();
    }
}
