package org.bryt.pages.selenide.main_menu;

import static com.codeborne.selenide.Selenide.$;

public class MainMenu {

    public void selectLogin(){
        selectMenuItem(MainMenuItem.LOGIN);
    }

    public void selectRegister(){
        selectMenuItem(MainMenuItem.REGISTER);
    }

    public void selectLogout(){
        selectMenuItem(MainMenuItem.LOGOUT);
    }

    public void selectWishList(){
        selectMenuItem(MainMenuItem.WISH_LIST);
    }

    public void selectShoppingCart(){
        selectMenuItem(MainMenuItem.SHOPPING_CART);
    }

    public void selectMyAccount(){
        selectMenuItem(MainMenuItem.ACCOUNT);
    }

    private void selectMenuItem(MainMenuItem menuItem){
       $("a.ico-" + menuItem.value()).click();
    }

}
