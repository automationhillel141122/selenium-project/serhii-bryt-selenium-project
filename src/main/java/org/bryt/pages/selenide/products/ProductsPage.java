package org.bryt.pages.selenide.products;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.bryt.pages.selenide.BasePage;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.$$x;

public class ProductsPage extends BasePage {
    public ProductsPage() {
        super();
    }

    public int getItemsCount(){
        return $$(".item-box").size();
    }

    public ProductsPage addProductToCart(String productName) {
        SelenideElement element =
                $$x("//div[@class='item-box']//div[@class='details']//a")
                        .find(Condition.exactText(productName));
        SelenideElement productItem = element.parent().parent();
        productItem.$(".buttons .product-box-add-to-cart-button").click();
        return new ProductsPage();
    }


    public List<ProductModel> getProducts(){
        List<ProductModel> productModels = new ArrayList<>();
        ElementsCollection elements =
                $$x("//div[@class='item-box']//div[@class='details']");
        for (SelenideElement element: elements){
            String productName = element.$x(".//h2/a").text();
            Long price = Long.parseLong(element.$(By.cssSelector(".price.actual-price"))
                    .text()
                    .substring(1)
                    .replaceAll("\\.", "")
                    .replaceAll(",",""));
            productModels.add(new ProductModel(productName, price));
        }
        return productModels;
    }

    public ProductPage selectProduct(String productName){
        $$x("//div[@class='item-box']//div[@class='details']//h2/a")
                .find(Condition.text(productName))
                .click();
        return new ProductPage();
    }


}
