package org.bryt.pages.selenide.products;

import com.codeborne.selenide.DownloadOptions;
import com.codeborne.selenide.FileDownloadMode;
import org.bryt.pages.selenide.BasePage;

import java.io.File;
import java.io.FileNotFoundException;

import static com.codeborne.selenide.Selenide.$;

public class ProductPage extends BasePage {
    public ProductPage() {
        super();
    }

    public File downloadSample(){
        try {
           return  $(".download-sample > a").download(DownloadOptions.using(FileDownloadMode.FOLDER));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
