package org.bryt.pages.main_menu;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.LoginPage;
import org.openqa.selenium.By;

public class MainMenu {

    public LoginPage selectLogin(){
        selectMenuItem(MainMenuItem.LOGIN);
        return new LoginPage();
    }

    public void selectRegister(){
        selectMenuItem(MainMenuItem.REGISTER);
    }

    public void selectLogout(){
        selectMenuItem(MainMenuItem.LOGOUT);
    }

    public void selectWishList(){
        selectMenuItem(MainMenuItem.WISH_LIST);
    }

    public void selectShoppingCart(){
        selectMenuItem(MainMenuItem.SHOPPING_CART);
    }

    public void selectMyAccount(){
        selectMenuItem(MainMenuItem.ACCOUNT);
    }

    private void selectMenuItem(MainMenuItem menuItem){
        WebdriverHolder.getInstance().getDriver()
                .findElement(By.cssSelector("a.ico-" + menuItem.value()))
                .click();
    }

}
