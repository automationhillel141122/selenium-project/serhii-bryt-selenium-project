package org.bryt.pages.compare;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.BasePage;
import org.bryt.pages.products.ProductModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class ComparePage extends BasePage {

    @FindBy(css = ".product-name")
    private WebElement productNameRow;

    @FindBy(css = ".product-price")
    private WebElement productPriceRow;

    public ComparePage() {
        PageFactory.initElements(WebdriverHolder.getInstance().getDriver(), this);
    }

    public List<ProductModel> getProducts() {
        List<ProductModel> result = new ArrayList<>();

        int size = productNameRow.findElements(By.xpath(".//td")).size();

        for (int i = 2; i <= size; i++) {
            String productName = productNameRow.findElement(By.xpath("./td[%s]".formatted(i))).getText();
            long price = Long.parseLong(productPriceRow.findElement(By.xpath("./td[%s]".formatted(i)))
                    .getText()
                    .substring(1)
                    .replaceAll("\\.", "")
                    .replaceAll(",", ""));
            result.add(new ProductModel(productName, price));
        }
        return result;
    }
}
