package org.bryt.pages.products;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ProductModel {
    private String productName;
    private Long price;

    public ProductModel(String productName, Long price) {
        this.productName = productName;
        this.price = price;
    }

    public String productName() {
        return productName;
    }

    public ProductModel setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Long price() {
        return price;
    }

    public ProductModel setPrice(Long price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "productName='" + productName + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ProductModel model = (ProductModel) o;

        return new EqualsBuilder().append(productName, model.productName).append(price, model.price).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(productName).append(price).toHashCode();
    }
}

