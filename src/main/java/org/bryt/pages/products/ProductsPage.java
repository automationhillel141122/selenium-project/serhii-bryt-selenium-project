package org.bryt.pages.products;

import org.bryt.driver.WebdriverHolder;
import org.bryt.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class ProductsPage extends BasePage {

    String itemBox = "//div[@class='item-box']//div[@class='details']//a[text()='%s']";

    public ProductsPage() {
        super();
    }

    public int getItemsCount(){
        return WebdriverHolder.getInstance().getDriver()
                .findElements(By.cssSelector(".item-box"))
                .size();
    }

    public ProductsPage addProductToCart(String productName) {
        return clickOnProductButton(productName, ProductActionButtons.ADD_TO_CART);
    }

    public ProductsPage addProductToWithList(String productName){
        return clickOnProductButton(productName, ProductActionButtons.ADD_TO_WISHLIST);
    }


    public List<ProductModel> getProducts(){
        List<ProductModel> productModels = new ArrayList<>();
        List<WebElement> elements = WebdriverHolder.getInstance().getDriver().findElements(By.xpath("//div[@class='item-box']//div[@class='details']"));
        for (WebElement element: elements){
            String productName = element.findElement(By.xpath(".//h2/a")).getText();
            Long price = Long.parseLong(element.findElement(By.cssSelector(".price.actual-price"))
                    .getText()
                    .substring(1)
                    .replaceAll("\\.", "")
                    .replaceAll(",",""));
            productModels.add(new ProductModel(productName, price));
        }
        return productModels;
    }

    public ProductsPage addProductToCompare(String productName) {
        return clickOnProductButton(productName, ProductActionButtons.ADD_TO_COMPARE);
    }

    private ProductsPage clickOnProductButton(String productName, ProductActionButtons actionButton){
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        WebElement element = driver.findElement(By.xpath(itemBox.formatted(productName)));
        WebElement productItem = element.findElement(By.xpath("./../.."));
        productItem.findElement(actionButton.buttonLocator()).click();
        waitTillSuccessMessageDisappear();
        return new ProductsPage();
    }

    private void waitTillSuccessMessageDisappear(){
        By messageBy = By.id("bar-notification");
        WebdriverHolder.getInstance().getWaiter().until(ExpectedConditions.visibilityOfElementLocated(messageBy));
        WebdriverHolder.getInstance().getWaiter().until(ExpectedConditions.invisibilityOfElementLocated(messageBy));
    }


    public ProductsPage sort(SortingDirection sortingDirection){
        WebElement selectSortingElement = WebdriverHolder.getInstance().getDriver().findElement(By.id("products-orderby"));
        Select select = new Select(selectSortingElement);
        select.selectByValue(sortingDirection.value().toString());
        sleep(2000);
        return new ProductsPage();
    }




}
