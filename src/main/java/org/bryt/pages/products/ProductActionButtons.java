package org.bryt.pages.products;

import org.openqa.selenium.By;

public enum ProductActionButtons {
    ADD_TO_CART(By.cssSelector(".buttons .product-box-add-to-cart-button")),
    ADD_TO_WISHLIST(By.cssSelector(".buttons .add-to-wishlist-button")),
    ADD_TO_COMPARE(By.cssSelector(".buttons .add-to-compare-list-button"));

    private By buttonLocator;

    ProductActionButtons(By buttonLocator) {
        this.buttonLocator = buttonLocator;
    }

    public By buttonLocator() {
        return buttonLocator;
    }
}