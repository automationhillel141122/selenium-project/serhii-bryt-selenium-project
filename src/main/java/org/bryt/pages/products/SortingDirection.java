package org.bryt.pages.products;

public enum SortingDirection {
    NAME_A_TO_Z(5),
    NAME_Z_TO_A(6),
    PRICE_LOW_TO_HIGH(10),
    PRICE_HIGH_TO_LOW(11);

    private Integer value;

    SortingDirection(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }


}
