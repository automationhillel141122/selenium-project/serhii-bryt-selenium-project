package org.bryt.steps;

import org.bryt.pages.BasePage;
import org.bryt.pages.products.ProductsPage;

public class ProductActionsSteps {
    public void addProductToCart(String category, String subcategory, String productName){
        ProductsPage productsPage;
        if (subcategory == null){
            new BasePage().categoryMenu().selectMainCategory(category);
        } else {
            new BasePage().categoryMenu().selectSubCategory(category, subcategory);
        }

        productsPage = new ProductsPage();

        productsPage.addProductToCart(productName);
    }
}
